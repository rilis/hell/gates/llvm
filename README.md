## LLVM and Clang proxy repository for hell

This is proxy repository for [LLVM](http://llvm.org/), which allow you to build and install LLVM and optionally Clang using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of LLVM/Clang using hell, have improvement idea, or want to request support for other versions of LLVM/Clang, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in LLVM/Clang itself please read and follow [llvm bug reporting guideline](http://llvm.org/docs/HowToSubmitABug.html), because here we don't do any kind of LLVM or Clang development.